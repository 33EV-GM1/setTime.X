#
# Generated - do not edit!
#
# NOCDDL
#
CND_BASEDIR=`pwd`
# default configuration
CND_ARTIFACT_DIR_default=dist/default/production
CND_ARTIFACT_NAME_default=setTime.X.production.hex
CND_ARTIFACT_PATH_default=dist/default/production/setTime.X.production.hex
CND_PACKAGE_DIR_default=${CND_DISTDIR}/default/package
CND_PACKAGE_NAME_default=settime.x.tar
CND_PACKAGE_PATH_default=${CND_DISTDIR}/default/package/settime.x.tar
# Red32 configuration
CND_ARTIFACT_DIR_Red32=dist/Red32/production
CND_ARTIFACT_NAME_Red32=setTime.X.production.hex
CND_ARTIFACT_PATH_Red32=dist/Red32/production/setTime.X.production.hex
CND_PACKAGE_DIR_Red32=${CND_DISTDIR}/Red32/package
CND_PACKAGE_NAME_Red32=settime.x.tar
CND_PACKAGE_PATH_Red32=${CND_DISTDIR}/Red32/package/settime.x.tar
# Blue256 configuration
CND_ARTIFACT_DIR_Blue256=dist/Blue256/production
CND_ARTIFACT_NAME_Blue256=setTime.X.production.hex
CND_ARTIFACT_PATH_Blue256=dist/Blue256/production/setTime.X.production.hex
CND_PACKAGE_DIR_Blue256=${CND_DISTDIR}/Blue256/package
CND_PACKAGE_NAME_Blue256=settime.x.tar
CND_PACKAGE_PATH_Blue256=${CND_DISTDIR}/Blue256/package/settime.x.tar
