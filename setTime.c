/*! \file  testUSBttl.c
 *
 *  \brief Set the clock from the PC
 *
 * **WARNING**
 * Serial port set to Arduino pins A7/A6 rather than default 0/1 so that
 * I2C pins are free for the DS1607 RTC.
 *
 *
 *  \author jjmcd
 *  \date March 14, 2016, 3:30 PM
 *
 * Software License Agreement
 * Copyright (c) 2016 by John J. McDonough, WB8RCR
 * This software is released under the GNU General Public License V2.
 * See the included files for a complete description.
 */
#include <xc.h>
#include <stdio.h>
#include <string.h>
#include "../include/basic33E.h"
#include "../include/configurationFuses.h"
#include "../include/LCD.h"
#include "../include/libcPlus.h"
#include "i2c.h"

#define DISPLAYONLY

//#define DS1307_ADDRESS 0x68
#define DS1307_ADDRESS 0xd0

unsigned char DS1307readByte( unsigned char, unsigned int);
void DS1307writeByte( unsigned char, unsigned int, unsigned char);

char szWork[32];    /* Work string */

char szDaysOfWeek[7][4] = {
 "Sun","Mon","Tue","Wed","Thu","Fri","Sat" };



/*! Display a startup banner */
/*! startupBanner displays a startup banner on the LCD for one
 *  second after which the display is cleared.  The banner shows the
 *  compile date and time on the right and the name of the application
 *  (broken into two lines) on the left.
 */
void startupBanner( void )
{
  LCDclear();
  LCDputs("set  ");
  LCDputs(__DATE__);
  LCDline2();
  LCDputs("Time    ");
  LCDputs(__TIME__);
  Delay_ms(1000);
  LCDclear();
}

unsigned char toHex( int nDecimal )
{
  return ( ((nDecimal/10)<<4) + (nDecimal%10) );
}


/*! testUSBttl - Test the Chinese USB to TTL converter */
/*! Mainline for testing the Chinese USB to TTL converter.  Text is
 *  sent to the serial port, each line preceded by a line number.
 *  At the end of each line the serial input is tested and if a
 *  character is available it is displayed on the LCD.
 */
int main(void)
{
  int nLineNumber;
  int nLCDposition;
  unsigned char cKeyedCharacter;
  int nStringPos;
  int nStarted;
  char szDateString[32];
  int nYear,nMonth,nDay;
  int nHour,nMinute,nSecond;
  int nDOW;

  /* Initialize the clock to 70 MIPS */
  initClock();

  /* Initialize the serial port */
  /* Map U1TX to RB4 (RP36, Arduino pin A6) */
  RPOR1bits.RP36R = 1; /* Map RP36 (A6) to U1TX */
  /* Map U1RX to RA4 (RP20, Arduino pin A7) */
  ANSELAbits.ANSA4 = 0; /* Not analog */
  RPINR18bits.U1RXR = 20; /* Map RP20 (A7) to U1RX */
  initializeSerial(1200, 0, 0);
  InitI2C();

  /* Initialize the LCD */
  OLEDinit();
  Delay_ms(500);
  startupBanner();
  Delay_ms(500);

  /* Initialize a couple of variables */
  nLineNumber=0;
  nLCDposition = 0;
  nStringPos = 0;
  nStarted = 0;
  memset(szDateString,0,sizeof(szDateString));

  while(1)
    {

      /* Check to see if there is a character on the input.
       * At this baud rate a manual typist won't outrun us */
      if ( U1STAbits.URXDA )
        {
          cKeyedCharacter = U1RXREG;    /* Get the character */
          LCDletter(cKeyedCharacter);   /* Send it to the LCD */
          nLCDposition++;               /* Remember our position */
          if ( nLCDposition==16 )
            {
              LCDline2();
              nLCDposition = 0x40;
            }

          if ( nLCDposition==0x50 )        /* Off the end of the screen? */
            {
              LCDhome();                /* Cursor home */
              nLCDposition = 0;         /* Remember we did that */
            }
          if ( nStarted )
            {
              szDateString[nStringPos] = cKeyedCharacter;
              nStringPos++;
              if ( cKeyedCharacter=='$' )
                {
                  szDateString[nStringPos] = '\0';
                  LCDclear();
                  //LCDputs(szDateString);
                  sscanf(szDateString,"%4d%2d%2d%2d%2d%2d$",&nYear,&nMonth,&nDay,
                          &nHour,&nMinute,&nSecond);
                  sprintf(szWork,"%4d-%02d-%02d      ",nYear,nMonth,nDay);
                  //LCDline2();
                  LCDputs(szWork);
                  //Delay_ms(1000);
                  sprintf(szWork,"%02d:%02d:%02d        ",nHour,nMinute,nSecond);
                  LCDline2();
                  LCDputs(szWork);
                  //Delay_ms(3000);
                  nStarted = 0;
                  nLCDposition = 0;
                  nStringPos = 0;
                  cKeyedCharacter = U1RXREG; /* Get the character */
                  cKeyedCharacter = U1RXREG; /* Get the character */
                  cKeyedCharacter = U1RXREG; /* Get the character */
                  cKeyedCharacter = U1RXREG; /* Get the character */
                  nDOW = daywk(nYear,nMonth,nDay);
                  sprintf(szWork,"%d",nDOW);
                  LCDposition(15);
                  LCDputs(szWork);
                  LCDposition(0x4d);
                  LCDputs(szDaysOfWeek[nDOW]);
                  DS1307writeByte(DS1307_ADDRESS,
                          (unsigned int) 2,
                          (toHex(nHour)&0x3f));
                  Delay_ms(10); // AT24C16 has 5 ms write cycle time
                  DS1307writeByte(DS1307_ADDRESS,
                          (unsigned int) 1,
                          (toHex(nMinute)&0x3f));
                  Delay_ms(10); // AT24C16 has 5 ms write cycle time
                  DS1307writeByte(DS1307_ADDRESS,
                          (unsigned int) 0,
                          (toHex(nSecond)&0x3f));
                  Delay_ms(10); // AT24C16 has 5 ms write cycle time
                  DS1307writeByte(DS1307_ADDRESS,
                          (unsigned int) 3,
                          (toHex(nDOW)&0x3f));
                  Delay_ms(10); // AT24C16 has 5 ms write cycle time
                  DS1307writeByte(DS1307_ADDRESS,
                          (unsigned int) 4,
                          (toHex(nDay)&0x3f));
                  Delay_ms(10); // AT24C16 has 5 ms write cycle time
                  DS1307writeByte(DS1307_ADDRESS,
                          (unsigned int) 5,
                          (toHex(nMonth)&0x3f));
                  Delay_ms(10); // AT24C16 has 5 ms write cycle time
                  DS1307writeByte(DS1307_ADDRESS,
                          (unsigned int) 6,
                          (toHex(nYear-2000)&0x3f));
                  Delay_ms(10); // AT24C16 has 5 ms write cycle time
                  //LCDclear();
                }
            }
          if (cKeyedCharacter == '!')
            {
              nStarted = 1;
              LCDclear();
              LCDputs("Started");
            }
        }
    }

  return 0;
}
