/*! \file i2c.h
 *
 *  \brief  Function prototypes and manifest constants for I2C
 *
 *
 *  \date 21-Nov-12
 *  \author JJMcD
 *
 */
#ifndef I2C__H
#define I2C__H

/*! Maximum length for get/put string */
#define PAGESIZE	32

/* Low Level Functions */

/*! Waits for bus to become Idle.   */
void IdleI2C(void);
/*! Generates an I2C Start Condition.   */
unsigned int StartI2C(void);
/*! Writes a byte out to the bus   */
void WriteI2C(unsigned char);
/*! Generates a bus stop condition.  */
unsigned int StopI2C(void);
/*! Generates a restart condition and optionally returns status.   */
unsigned int RestartI2C(void);
/*! Initializes the I2C peripheral.   */
void InitI2C(void);
/*! Read a single byte from Bus.  */
unsigned int getI2C(void);
/*! Generates an Acknowledge.   */
void AckI2C(void);
/*! Return the Acknowledge status on the bus   */
unsigned int ACKstatusI2C( void );
/*! Generates a NO Acknowledge on the Bus   */
void NotAckI2C(void);

/* Calculate the value for the I2C baud rate generator */

/*! Instruction cycle time */
#define FCY 70000000
/*! I2C baud rate */
#define FSCK 400000
/*! Value for baud rate generator */
//#define BRGVAL ( (FCY/FSCK) - (FCY/1111111) ) - 1
// 0x360 appears to be very close to 40 kHz
//#define BRGVAL (0x360)
// 0x57 should be close to 400 kHz, standard I2C rate
// but it measures closer to 29 kHz and getting quite rounded
#define BRGVAL (0x45)

#endif
